var searchData=
[
  ['calcangle_11',['calcAngle',['../namespacemain__final.html#abbbe2909d28f929836a4c0eba73d713a',1,'main_final']]],
  ['calcvelocity_12',['calcVelocity',['../namespacemain__final.html#a42de65bcaf9bfef609dc85fe676cff3d',1,'main_final']]],
  ['celsius_13',['celsius',['../classmcp9808_1_1mcp9808.html#a2aa3aa0a45168d84968c7a36d7cdd542',1,'mcp9808::mcp9808']]],
  ['check_14',['check',['../classmcp9808_1_1mcp9808.html#a88d1f2c040cc310d72a26e0cda0c99bc',1,'mcp9808::mcp9808']]],
  ['compute_15',['compute',['../class_controller_1_1_controller.html#a99a630f023cc59a1a305043cf58dfde6',1,'Controller::Controller']]],
  ['controller_16',['Controller',['../class_controller_1_1_controller.html',1,'Controller.Controller'],['../namespace_controller.html',1,'Controller']]],
  ['count_17',['count',['../namespacemain.html#ace2fede70217ca57d2a06a4e05de84e1',1,'main.count()'],['../namespacemain___lab2.html#ac20e12d58c73d8d704fcec38f99d4f29',1,'main_Lab2.count()']]],
  ['count_5fisr_18',['count_isr',['../namespacemain.html#ae6318f7ba40462612a2488e5b2af11ac',1,'main.count_isr()'],['../namespacemain___lab2.html#a60d9105fa1a1f15af1d35c6e3f0b9586',1,'main_Lab2.count_isr()']]],
  ['current_5fcount_19',['current_count',['../class_encoder_1_1_encoder.html#a84946b5625f65ec43affac2e06670936',1,'Encoder::Encoder']]],
  ['current_5fpos_20',['current_pos',['../class_encoder_1_1_encoder.html#a2b1fd649e1f2ca788a1ccc2c0e93f4f2',1,'Encoder::Encoder']]]
];
