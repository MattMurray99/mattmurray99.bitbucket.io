var searchData=
[
  ['p1_63',['P1',['../class_encoder_1_1_encoder.html#af47ce3cbcafa8f9efc3f0c1f5df0e0f9',1,'Encoder::Encoder']]],
  ['p1y_64',['P1y',['../namespacemain__final.html#a96fc1a4e9818728276517eb3b35e742e',1,'main_final']]],
  ['p2_65',['P2',['../class_encoder_1_1_encoder.html#aac9f3438128e3c2bb5e9d9712d940350',1,'Encoder::Encoder']]],
  ['p_5fxm_66',['P_xm',['../class_touch___driver_1_1_touch___driver.html#acf015fee1c7e978497f7f37146f7674a',1,'Touch_Driver.Touch_Driver.P_xm()'],['../namespacemain.html#a5f2b334e57240337ad42f4032d50758f',1,'main.P_xm()'],['../namespacemain___lab7.html#a46f9ef84c61c5e5f26a80266da239908',1,'main_Lab7.P_xm()']]],
  ['p_5fxp_67',['P_xp',['../class_touch___driver_1_1_touch___driver.html#ab6d6f71a8b23308e28f5ff82dee58531',1,'Touch_Driver.Touch_Driver.P_xp()'],['../namespacemain.html#a126bdaa7d8cd4bbe28313d3e3404b198',1,'main.P_xp()'],['../namespacemain___lab7.html#a4ef4f85bdbdb11380c6d6bc2a6ee7269',1,'main_Lab7.P_xp()']]],
  ['p_5fym_68',['P_ym',['../class_touch___driver_1_1_touch___driver.html#a5fab6432e2e486cab95be5fba0973bd1',1,'Touch_Driver.Touch_Driver.P_ym()'],['../namespacemain.html#ac7d9f96d752a89bd4ca88e879aea38bd',1,'main.P_ym()'],['../namespacemain___lab7.html#ac45b6051868fa91afa97162c416f5340',1,'main_Lab7.P_ym()']]],
  ['p_5fyp_69',['P_yp',['../class_touch___driver_1_1_touch___driver.html#a018e609638c4aaf06cab577f8b24770a',1,'Touch_Driver.Touch_Driver.P_yp()'],['../namespacemain.html#a5a7c4dc36f5146e94e442d8c2c99ab3b',1,'main.P_yp()'],['../namespacemain___lab7.html#aaff51db758c48c510f87268111288d50',1,'main_Lab7.P_yp()']]],
  ['pin1_70',['pin1',['../class_motor_driver_1_1_motor_driver.html#ac7f14dc68674439b82595de8f824f81a',1,'MotorDriver::MotorDriver']]],
  ['pin2_71',['pin2',['../class_motor_driver_1_1_motor_driver.html#ab52ee80450764137469320de43b21965',1,'MotorDriver::MotorDriver']]],
  ['pinsleep_72',['pinSLEEP',['../class_motor_driver_1_1_motor_driver.html#a7afa5e66732f4a168ea85eb6037ab8b1',1,'MotorDriver::MotorDriver']]],
  ['pos_73',['pos',['../class_controller_1_1_controller.html#a13090e584fc4c0ccb3df4075f284bb60',1,'Controller::Controller']]],
  ['posdot_74',['posDot',['../class_controller_1_1_controller.html#aa2aa368537eb8dee2d15e8c258154de3',1,'Controller::Controller']]],
  ['prev_5fcount_75',['prev_count',['../class_encoder_1_1_encoder.html#a8a34bf2aa73098861a7c4c8f8070684a',1,'Encoder::Encoder']]],
  ['prev_5fpos_76',['prev_pos',['../class_encoder_1_1_encoder.html#a9e53318f874983263232ca82ccbc0e01',1,'Encoder::Encoder']]],
  ['printwelcome_77',['printWelcome',['../namespace_lab1.html#a5705c02c11ac6395e136644c46822172',1,'Lab1']]]
];
