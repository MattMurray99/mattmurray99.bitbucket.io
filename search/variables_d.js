var searchData=
[
  ['scaled_5fvalues_208',['scaled_values',['../namespace_lab3___u_i.html#ae6b4cd68b1a4b045a908391f3667d13c',1,'Lab3_UI']]],
  ['sensor_5fc_5fval_209',['sensor_C_val',['../namespacemain.html#a740cbe69f282418b365a6eee386661a1',1,'main.sensor_C_val()'],['../namespacemain___lab4.html#a95c91a9999a90887711d3469a640834c',1,'main_Lab4.sensor_C_val()']]],
  ['sensor_5ff_5fval_210',['sensor_F_val',['../namespacemain.html#abda3ebadb9c2ddf0ad679c8cb43fe692',1,'main.sensor_F_val()'],['../namespacemain___lab4.html#a88d630b80712fda05f63b794937ba2b3',1,'main_Lab4.sensor_F_val()']]],
  ['ssx_211',['ssX',['../namespacemain__final.html#abcfd9aaaf44ce027255caa5582d76498',1,'main_final']]],
  ['ssy_212',['ssY',['../namespacemain__final.html#ade91bdb0128c309600a6343e67b9b765',1,'main_final']]],
  ['state_213',['state',['../namespace_lab1.html#a983c614200fcdbe6e3ab0a8ad4c41613',1,'Lab1']]]
];
