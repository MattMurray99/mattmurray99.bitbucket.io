var searchData=
[
  ['access_5fbit_1',['access_bit',['../namespacemcp9808.html#ab942ff574cdda8a2561d5d9d6ccbe82b',1,'mcp9808']]],
  ['adc_2',['adc',['../namespacemain.html#afac357daa82cb1f4a2ca28df4f60bb14',1,'main.adc()'],['../namespacemain___lab3.html#aa060bfd96838b0bcb7c5dfa296c5a12d',1,'main_Lab3.adc()']]],
  ['adc_5fflag_3',['adc_flag',['../namespacemain.html#af0b06dd0c9463ccb275614435db7363d',1,'main.adc_flag()'],['../namespacemain___lab3.html#a6830d02a0fdec0c2a078e8ffc08cf4c6',1,'main_Lab3.adc_flag()']]],
  ['adi_4',['adi',['../classmcp9808_1_1mcp9808.html#a2a43a7ceeef209618cad793973396e62',1,'mcp9808.mcp9808.adi()'],['../namespacemain.html#a72b719087980d037d504fbb51f97a140',1,'main.adi()'],['../namespacemain___lab4.html#a12dded8d247fc31c9156be6d190e0e25',1,'main_Lab4.adi()'],['../namespacemcp9808.html#a998791b7d46e7e7d7cb0295b498e53c4',1,'mcp9808.adi()']]],
  ['allscan_5',['Allscan',['../class_touch___driver_1_1_touch___driver.html#a1004129fd35da47d0e6862100b430f3e',1,'Touch_Driver.Touch_Driver.Allscan(self)'],['../class_touch___driver_1_1_touch___driver.html#a1004129fd35da47d0e6862100b430f3e',1,'Touch_Driver.Touch_Driver.Allscan(self)']]],
  ['aug_5fbuffer_6',['aug_buffer',['../namespacemain.html#ac271227338480fc1c12179a1b8eb5784',1,'main.aug_buffer()'],['../namespacemain___lab3.html#afa9baa914f408c090db090add15739a4',1,'main_Lab3.aug_buffer()']]]
];
