var searchData=
[
  ['raw_5fvalues_78',['raw_values',['../namespace_lab3___u_i.html#a7775cdf2026633854af465782619a7b9',1,'Lab3_UI']]],
  ['reaction_79',['reaction',['../namespacemain.html#af93e4a6b391b7237ad6a11f7aa123865',1,'main.reaction()'],['../namespacemain___lab2.html#a7c44f48320e622e8d0c59936b1a64179',1,'main_Lab2.reaction()']]],
  ['readadc_5fisr_80',['readADC_isr',['../namespacemain.html#ae2512f3b6e2f9cd81284b706ff9307ed',1,'main.readADC_isr()'],['../namespacemain___lab3.html#af49bf648b00729e4c3d6d5e700c28aa0',1,'main_Lab3.readADC_isr()']]],
  ['reset_81',['reset',['../namespacemain__final.html#aedf58272feebdeb5f69c0cd20da408be',1,'main_final']]],
  ['returnkey_82',['returnkey',['../namespace_lab3___u_i.html#affb65e65c9803bdf62cba72a265a9570',1,'Lab3_UI']]]
];
