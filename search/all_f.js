var searchData=
[
  ['scaled_5fvalues_83',['scaled_values',['../namespace_lab3___u_i.html#ae6b4cd68b1a4b045a908391f3667d13c',1,'Lab3_UI']]],
  ['sendchar_84',['sendChar',['../namespace_lab3___u_i.html#ac3113fb53c160167ae1631f9a132fde6',1,'Lab3_UI']]],
  ['sensor_5fc_5fval_85',['sensor_C_val',['../namespacemain.html#a740cbe69f282418b365a6eee386661a1',1,'main.sensor_C_val()'],['../namespacemain___lab4.html#a95c91a9999a90887711d3469a640834c',1,'main_Lab4.sensor_C_val()']]],
  ['sensor_5ff_5fval_86',['sensor_F_val',['../namespacemain.html#abda3ebadb9c2ddf0ad679c8cb43fe692',1,'main.sensor_F_val()'],['../namespacemain___lab4.html#a88d630b80712fda05f63b794937ba2b3',1,'main_Lab4.sensor_F_val()']]],
  ['set_5fduty_87',['set_duty',['../class_motor_driver_1_1_motor_driver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver.MotorDriver.set_duty(self, duty)'],['../class_motor_driver_1_1_motor_driver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver.MotorDriver.set_duty(self, duty)']]],
  ['setposition_88',['setPosition',['../class_encoder_1_1_encoder.html#aae8ed6cb138bf8e83d7947bb137345ed',1,'Encoder.Encoder.setPosition(self, pos)'],['../class_encoder_1_1_encoder.html#aae8ed6cb138bf8e83d7947bb137345ed',1,'Encoder.Encoder.setPosition(self, pos)']]],
  ['ssx_89',['ssX',['../namespacemain__final.html#abcfd9aaaf44ce027255caa5582d76498',1,'main_final']]],
  ['ssy_90',['ssY',['../namespacemain__final.html#ade91bdb0128c309600a6343e67b9b765',1,'main_final']]],
  ['state_91',['state',['../namespace_lab1.html#a983c614200fcdbe6e3ab0a8ad4c41613',1,'Lab1']]]
];
