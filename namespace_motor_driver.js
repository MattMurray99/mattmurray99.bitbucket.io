var namespace_motor_driver =
[
    [ "MotorDriver", "class_motor_driver_1_1_motor_driver.html", "class_motor_driver_1_1_motor_driver" ],
    [ "IN1X_pin", "namespace_motor_driver.html#a046d822e05196ae2aafd900213da7b64", null ],
    [ "IN1Y_pin", "namespace_motor_driver.html#adc30951a777fc40fc65bdd57f29c8e82", null ],
    [ "IN2X_pin", "namespace_motor_driver.html#aac48541a958512be97af7b54bdb84155", null ],
    [ "IN2Y_pin", "namespace_motor_driver.html#a8c41bacefcd612c76512e34416e91cb4", null ],
    [ "motoX", "namespace_motor_driver.html#a3ced03a6a6518f7542c1501dc1cba130", null ],
    [ "motoY", "namespace_motor_driver.html#a6193de434d9b0a1bf40e310a84bb8cf6", null ],
    [ "sleep_pin", "namespace_motor_driver.html#a032bc2772be179ac9226a6d4ec4f3e5e", null ],
    [ "timer", "namespace_motor_driver.html#a00d95c9118f23cfe3b11254b49366818", null ]
];