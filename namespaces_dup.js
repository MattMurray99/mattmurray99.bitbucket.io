var namespaces_dup =
[
    [ "Controller", "namespace_controller.html", "namespace_controller" ],
    [ "Encoder", "namespace_encoder.html", "namespace_encoder" ],
    [ "Lab1", "namespace_lab1.html", [
      [ "DivNSubtract", "namespace_lab1.html#a797d8e5a4656124601cf7a9237bebf9d", null ],
      [ "getChange", "namespace_lab1.html#a3311d416a1f0321d0ccb07ed34f94fc2", null ],
      [ "kb_cb", "namespace_lab1.html#a6fe3634c1224c39df50be648cd276595", null ],
      [ "printWelcome", "namespace_lab1.html#a5705c02c11ac6395e136644c46822172", null ],
      [ "balance", "namespace_lab1.html#a24579f1136f51d760142dd7090ba9bbf", null ],
      [ "callback", "namespace_lab1.html#ac83d4a2ded1323392f81586254b73335", null ],
      [ "change", "namespace_lab1.html#a98ca37d6621be24ca6799e1bd0965713", null ],
      [ "last_key", "namespace_lab1.html#a1866809cdfe46b562ac953ac4679e945", null ],
      [ "state", "namespace_lab1.html#a983c614200fcdbe6e3ab0a8ad4c41613", null ]
    ] ],
    [ "Lab3_UI", "namespace_lab3___u_i.html", [
      [ "sendChar", "namespace_lab3___u_i.html#ac3113fb53c160167ae1631f9a132fde6", null ],
      [ "ax", "namespace_lab3___u_i.html#a76c01ba793636a5997bc94c58c20baad", null ],
      [ "color", "namespace_lab3___u_i.html#a04b279c2c4020a5134368f59e1e64956", null ],
      [ "fig", "namespace_lab3___u_i.html#a5b6992c8244b4b2636cbe60317a68eb4", null ],
      [ "floatvalue", "namespace_lab3___u_i.html#a90519a75788c4048149c73b24ccd3ba6", null ],
      [ "i", "namespace_lab3___u_i.html#ada03cbfd14f50b7e036b1e05a465ac8c", null ],
      [ "newline", "namespace_lab3___u_i.html#a4d661b7e325cb3272e1382aecd337d45", null ],
      [ "num_lines", "namespace_lab3___u_i.html#a4ffa2d9a654aba3db6dff58207466e64", null ],
      [ "raw_values", "namespace_lab3___u_i.html#a7775cdf2026633854af465782619a7b9", null ],
      [ "returnkey", "namespace_lab3___u_i.html#affb65e65c9803bdf62cba72a265a9570", null ],
      [ "scaled_values", "namespace_lab3___u_i.html#ae6b4cd68b1a4b045a908391f3667d13c", null ],
      [ "ser", "namespace_lab3___u_i.html#a3217c0b9e60b715bb7a764461b0195dd", null ],
      [ "suppress", "namespace_lab3___u_i.html#a2481352c2b0132d0a1715171aaf9eead", null ],
      [ "timestamp_values", "namespace_lab3___u_i.html#a2ef4dcd88699e5253d595875aee6859e", null ],
      [ "value", "namespace_lab3___u_i.html#afc1638d8e3fd19b837a6c0c03b132886", null ],
      [ "writer", "namespace_lab3___u_i.html#a496042cc69649b08d2af43ba4500b9a1", null ]
    ] ],
    [ "main", "namespacemain.html", [
      [ "clearFault_isr", "namespacemain.html#a690db9baf3005ce4af2669306a68b048", null ],
      [ "count_isr", "namespacemain.html#ae6318f7ba40462612a2488e5b2af11ac", null ],
      [ "readADC_isr", "namespacemain.html#ae2512f3b6e2f9cd81284b706ff9307ed", null ],
      [ "setFault_isr", "namespacemain.html#adb11d47154d0eef43e52adc8d220386c", null ],
      [ "adc", "namespacemain.html#afac357daa82cb1f4a2ca28df4f60bb14", null ],
      [ "adc_flag", "namespacemain.html#af0b06dd0c9463ccb275614435db7363d", null ],
      [ "adi", "namespacemain.html#a72b719087980d037d504fbb51f97a140", null ],
      [ "aug_buffer", "namespacemain.html#ac271227338480fc1c12179a1b8eb5784", null ],
      [ "blinko", "namespacemain.html#a3ca5d065aab309d79adf1ed4e4083e87", null ],
      [ "btn_pin", "namespacemain.html#a007c0c355442ae5d48220acdd3e739fe", null ],
      [ "buffer", "namespacemain.html#a2888d28fa35f990cfa097aa3f9ecb184", null ],
      [ "count", "namespacemain.html#ace2fede70217ca57d2a06a4e05de84e1", null ],
      [ "delay", "namespacemain.html#aafb6c51fc28a87c9d8fe153e614c65ee", null ],
      [ "end_time", "namespacemain.html#af5f82ecbf21dc9e0582f9c4e03fa5b98", null ],
      [ "extint", "namespacemain.html#aed616b6b5cf07825f2f9f2e33ab071a5", null ],
      [ "extint1", "namespacemain.html#ac3b0a410083cae985fbbcd0e8becda9c", null ],
      [ "extint2", "namespacemain.html#acadffb59925e9ddd70b14cc4050d365c", null ],
      [ "fault_pin", "namespacemain.html#ad41292e51f6d37cb23c584c005f315a5", null ],
      [ "i", "namespacemain.html#ade6e2febf33b88a767c2e85902add210", null ],
      [ "i2c", "namespacemain.html#ab2bba10cbf0946121616503df3750664", null ],
      [ "IN1X_pin", "namespacemain.html#a7564eb3df4095f8aa1121f9bac2af7bb", null ],
      [ "IN1Y_pin", "namespacemain.html#ac337be39ffb9f78934d50e1a8a302fab", null ],
      [ "IN2X_pin", "namespacemain.html#a92e6a5ca910ae532d30ddba8794b9328", null ],
      [ "IN2Y_pin", "namespacemain.html#af46ec4cbd3d390573b06af1c2843c972", null ],
      [ "internal_temp", "namespacemain.html#ab66f0bdda00f6361a4b6077d83caf360", null ],
      [ "key", "namespacemain.html#a832185a351506083e49f83201cbc8433", null ],
      [ "L", "namespacemain.html#a693518618190dcfab7628fe24de4e87a", null ],
      [ "mcp", "namespacemain.html#a0ce8364e33e8481fd92c82a93e8d8392", null ],
      [ "motoX", "namespacemain.html#a6ff1533669ed1b29e3f0bff8a84fec73", null ],
      [ "motoY", "namespacemain.html#a95221b5ace0fc3e4bcc0ad6f0f9d6066", null ],
      [ "P_xm", "namespacemain.html#a5f2b334e57240337ad42f4032d50758f", null ],
      [ "P_xp", "namespacemain.html#a126bdaa7d8cd4bbe28313d3e3404b198", null ],
      [ "P_ym", "namespacemain.html#ac7d9f96d752a89bd4ca88e879aea38bd", null ],
      [ "P_yp", "namespacemain.html#a5a7c4dc36f5146e94e442d8c2c99ab3b", null ],
      [ "reaction", "namespacemain.html#af93e4a6b391b7237ad6a11f7aa123865", null ],
      [ "reading", "namespacemain.html#a598d0cf70a4b255bec6a82b86c40a23d", null ],
      [ "sensor_C_val", "namespacemain.html#a740cbe69f282418b365a6eee386661a1", null ],
      [ "sensor_F_val", "namespacemain.html#abda3ebadb9c2ddf0ad679c8cb43fe692", null ],
      [ "sleep_pin", "namespacemain.html#ad2c21c7e101ff53d294ce0e33d82fe29", null ],
      [ "start_time", "namespacemain.html#ae57958345b17f9ca8597330ba07e1a1c", null ],
      [ "td", "namespacemain.html#ac627b5ec9663089b5f19f31073445fde", null ],
      [ "tim", "namespacemain.html#a232f417b17e7cf7003f080763c8f511f", null ],
      [ "time", "namespacemain.html#ad0d873ced902b7535cdb725a4a68ade3", null ],
      [ "time_val", "namespacemain.html#ab0711532fed61f4f8826367ce8825061", null ],
      [ "timeout", "namespacemain.html#a491256c6d147ef04cae12a6afc692ff2", null ],
      [ "timer", "namespacemain.html#a4fe20a9d36fdc5b4ea50d88f43f6ed54", null ],
      [ "total_time", "namespacemain.html#afe09734cd0196e09806fcc0f7ed9e895", null ],
      [ "tries", "namespacemain.html#af4d201a5d7a4ed75055558763de39a79", null ],
      [ "uart", "namespacemain.html#a20f34167ce94fc3bd45d24f2af4d827a", null ],
      [ "val", "namespacemain.html#af30a285a2222c0a986b33b478f9bf927", null ],
      [ "W", "namespacemain.html#aa2c6a67ed3a5c1f0cdbdb4f55ca4f20c", null ]
    ] ],
    [ "main_final", "namespacemain__final.html", [
      [ "calcAngle", "namespacemain__final.html#abbbe2909d28f929836a4c0eba73d713a", null ],
      [ "calcVelocity", "namespacemain__final.html#a42de65bcaf9bfef609dc85fe676cff3d", null ],
      [ "nfault", "namespacemain__final.html#ac607f9a0cbfb97c6a51cd16fb4018c74", null ],
      [ "reset", "namespacemain__final.html#aedf58272feebdeb5f69c0cd20da408be", null ],
      [ "angle_data", "namespacemain__final.html#a48e33cea7924f77866c1d839b7f82202", null ],
      [ "AVx", "namespacemain__final.html#a81cea112de828eed5c6adae5d1ef87f8", null ],
      [ "AVy", "namespacemain__final.html#aaf032b1d443d269bd39c41cca7caad3d", null ],
      [ "Ax", "namespacemain__final.html#a4a7c95ea1ac8658a030d2193db95cd0a", null ],
      [ "Ay", "namespacemain__final.html#acd507240bf77ca784ce341dbdfa3d7bc", null ],
      [ "dutyX", "namespacemain__final.html#a4adb7efd7eefe10f7ac0e991b2e23e04", null ],
      [ "dutyY", "namespacemain__final.html#a0de7d46c7cb0dddf059b03843cd743ce", null ],
      [ "encx", "namespacemain__final.html#a2241ff2359203a0aa54eb22c33e2726b", null ],
      [ "ency", "namespacemain__final.html#aaa875d33eb2d4166fee67f0418b6d957", null ],
      [ "extint1", "namespacemain__final.html#af28f29ca109d25c5fce97469df966729", null ],
      [ "extint2", "namespacemain__final.html#ab68a173134b69698eec11c490d8a660b", null ],
      [ "fault", "namespacemain__final.html#a86952750ac159075fa38feca435a3063", null ],
      [ "motoX", "namespacemain__final.html#a7820af23c74f9fbe1df003cc12062006", null ],
      [ "motoY", "namespacemain__final.html#a4bc7159e14215cffb36dfcf534e799a3", null ],
      [ "P1x", "namespacemain__final.html#a12b5f7c16052021e9cb91173dad8f6b0", null ],
      [ "P1y", "namespacemain__final.html#a96fc1a4e9818728276517eb3b35e742e", null ],
      [ "P2x", "namespacemain__final.html#a0e2536e922b6f16d85aaf3498b48fa40", null ],
      [ "P2y", "namespacemain__final.html#a3dd73e8a3a1e3f0d74c1ebd4a918f5fa", null ],
      [ "ssX", "namespacemain__final.html#abcfd9aaaf44ce027255caa5582d76498", null ],
      [ "ssY", "namespacemain__final.html#ade91bdb0128c309600a6343e67b9b765", null ],
      [ "td", "namespacemain__final.html#ad29907f76cc68ad2d4ca8bb3344d5c2c", null ],
      [ "timerx", "namespacemain__final.html#a67ec96e9b56468c6fa20d0c2ff3d7ee3", null ],
      [ "timery", "namespacemain__final.html#a70994ca60eebc449a16b99821de7cdc8", null ],
      [ "touch_data", "namespacemain__final.html#a6f130481f5e9a18a2e2dc6ce42e774b3", null ],
      [ "vcp", "namespacemain__final.html#af5181feb7fa7290a1b531a92a7650300", null ],
      [ "Vx", "namespacemain__final.html#a6d589557d471650bcd60164af73aa9c2", null ],
      [ "Vy", "namespacemain__final.html#a11a33ad30baa41562b3d2d70bd9a636f", null ],
      [ "x", "namespacemain__final.html#aa789eaaa075c191561c08e25d577fd99", null ],
      [ "y", "namespacemain__final.html#afe72eb67b84f1a2f3b6dfbfa7f9e783d", null ],
      [ "Z", "namespacemain__final.html#ac29b55066e6441e9d8045685549b3fed", null ]
    ] ],
    [ "main_Lab2", "namespacemain___lab2.html", [
      [ "count_isr", "namespacemain___lab2.html#a60d9105fa1a1f15af1d35c6e3f0b9586", null ],
      [ "blinko", "namespacemain___lab2.html#a09a5440b03c922d6fd55bd1a18a3b177", null ],
      [ "count", "namespacemain___lab2.html#ac20e12d58c73d8d704fcec38f99d4f29", null ],
      [ "delay", "namespacemain___lab2.html#a509100c38f8008b2f7eca0c8988601e7", null ],
      [ "extint", "namespacemain___lab2.html#a213767c329992b01e3ac1109334f5158", null ],
      [ "reaction", "namespacemain___lab2.html#a7c44f48320e622e8d0c59936b1a64179", null ],
      [ "tim", "namespacemain___lab2.html#a2e9be60db6cebe951186f3762a6f705d", null ],
      [ "tries", "namespacemain___lab2.html#ab7572bcd540839676ced5fca2e19ff0b", null ]
    ] ],
    [ "main_Lab3", "namespacemain___lab3.html", [
      [ "readADC_isr", "namespacemain___lab3.html#af49bf648b00729e4c3d6d5e700c28aa0", null ],
      [ "adc", "namespacemain___lab3.html#aa060bfd96838b0bcb7c5dfa296c5a12d", null ],
      [ "adc_flag", "namespacemain___lab3.html#a6830d02a0fdec0c2a078e8ffc08cf4c6", null ],
      [ "aug_buffer", "namespacemain___lab3.html#afa9baa914f408c090db090add15739a4", null ],
      [ "blinko", "namespacemain___lab3.html#ab3ff3ebfe5bc4779acd524c091c091da", null ],
      [ "buffer", "namespacemain___lab3.html#a8788ac0ea6940662791551efef262b0c", null ],
      [ "extint", "namespacemain___lab3.html#afd43eeb5824d75d3dff1b29cea2d2437", null ],
      [ "key", "namespacemain___lab3.html#acb40622d11a2125d705ea57f2fd2aef2", null ],
      [ "uart", "namespacemain___lab3.html#acc36f718c5a371e286a0d72e53607ea9", null ]
    ] ],
    [ "main_Lab4", "namespacemain___lab4.html", [
      [ "adc", "namespacemain___lab4.html#ae9ba2577a7c9cb02f7753553befe8e84", null ],
      [ "adi", "namespacemain___lab4.html#a12dded8d247fc31c9156be6d190e0e25", null ],
      [ "blinko", "namespacemain___lab4.html#ab5ee58abbfc133f5b382dd8b919c0aa8", null ],
      [ "i", "namespacemain___lab4.html#a9890de04f28b8cfe7336c78ecca55558", null ],
      [ "i2c", "namespacemain___lab4.html#ab6d5da9096e401b5adacfed3fb266585", null ],
      [ "internal_temp", "namespacemain___lab4.html#a73597218c4daf4de97eff640340d4173", null ],
      [ "mcp", "namespacemain___lab4.html#a7cc12de462f35a2ff891bdcc2e464fd8", null ],
      [ "sensor_C_val", "namespacemain___lab4.html#a95c91a9999a90887711d3469a640834c", null ],
      [ "sensor_F_val", "namespacemain___lab4.html#a88d630b80712fda05f63b794937ba2b3", null ],
      [ "time", "namespacemain___lab4.html#a59689842876697f87e9afa02a23d1aba", null ],
      [ "time_val", "namespacemain___lab4.html#a35f53cfa4b489a5aae45b332444c2f06", null ],
      [ "timeout", "namespacemain___lab4.html#a4bee96f7705eabaf044855c79e6109f6", null ]
    ] ],
    [ "main_Lab7", "namespacemain___lab7.html", [
      [ "end_time", "namespacemain___lab7.html#a76b4b2e104c7896c6b27bdf247787b72", null ],
      [ "L", "namespacemain___lab7.html#a2f8883a92789385a7ae21581af6b8f5a", null ],
      [ "P_xm", "namespacemain___lab7.html#a46f9ef84c61c5e5f26a80266da239908", null ],
      [ "P_xp", "namespacemain___lab7.html#a4ef4f85bdbdb11380c6d6bc2a6ee7269", null ],
      [ "P_ym", "namespacemain___lab7.html#ac45b6051868fa91afa97162c416f5340", null ],
      [ "P_yp", "namespacemain___lab7.html#aaff51db758c48c510f87268111288d50", null ],
      [ "reading", "namespacemain___lab7.html#a367878fcaa922d619bce29b75b9e69e4", null ],
      [ "start_time", "namespacemain___lab7.html#aedf462ec9e66b4491acfef7d36546dfd", null ],
      [ "td", "namespacemain___lab7.html#ad619a88e40d564f65ae455c97cae4372", null ],
      [ "total_time", "namespacemain___lab7.html#a3baf77147152e7ef866fbe3204d01bb6", null ],
      [ "W", "namespacemain___lab7.html#a44a75e82248c4ec6a5da5ab36a99c7f0", null ]
    ] ],
    [ "mcp9808", "namespacemcp9808.html", "namespacemcp9808" ],
    [ "MotorDriver", "namespace_motor_driver.html", "namespace_motor_driver" ],
    [ "test", null, [
      [ "inv", "_lab3_2test_8py.html#a6c1905487fde68e36dc44dd453fb03a4", null ],
      [ "key", "_lab3_2test_8py.html#a7573407cf991ca34f068886b86251b7c", null ],
      [ "ser", "_lab3_2test_8py.html#acb217bd4ff73a22557532a823d4cda2a", null ]
    ] ],
    [ "Touch_Driver", "namespace_touch___driver.html", "namespace_touch___driver" ]
];