var namespace_encoder =
[
    [ "Encoder", "class_encoder_1_1_encoder.html", "class_encoder_1_1_encoder" ],
    [ "encx", "namespace_encoder.html#a09f6ceedd4a1997d03d34ad5d927f352", null ],
    [ "ency", "namespace_encoder.html#a495175110decb74dacc06adea4d1d6ec", null ],
    [ "P1x", "namespace_encoder.html#ade812adb4ba1be27ad3a27ff1f67c60c", null ],
    [ "P1y", "namespace_encoder.html#ab76d7633a79d3b75c477ee5df0a38aa6", null ],
    [ "P2x", "namespace_encoder.html#a0120a758d9b53649e3d0c1afd8028182", null ],
    [ "P2y", "namespace_encoder.html#a99c333ad6741a7c1f4393115cdf21292", null ],
    [ "posx", "namespace_encoder.html#a86fb4aae0b64d80af624a888032cfcd3", null ],
    [ "posy", "namespace_encoder.html#ae4af432cba019be0e5d24865a057b1f0", null ],
    [ "timerx", "namespace_encoder.html#aed1cacbf4fc00cc6a7b9a9fb31411fa3", null ],
    [ "timery", "namespace_encoder.html#af9cd6d91c2a1623a71dc15362e0b5771", null ]
];